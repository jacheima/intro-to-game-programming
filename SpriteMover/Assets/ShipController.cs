﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

    public Vector2 center  =new Vector2(0.0f, -4.5f);
    private bool pPressed;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
	    {
	        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
	        {
	            Vector2 pos = transform.position;
	            pos.x--;
	            transform.position = pos;
	        }

	        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
	        {
	            Vector2 pos = transform.position;
	            pos.x++;
	            transform.position = pos;
	        }

        }
	    else
	    {
	        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
	        {
	            Vector2 pos = transform.position;
	            pos.x--;
	            transform.position = pos;
	        }

	        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
	        {
	            Vector2 pos = transform.position;
	            pos.x++;
	            transform.position = pos;
	        }
        }

	    if (Input.GetKeyDown(KeyCode.Space))
	    {
	        transform.position = center;

	    }


	    
	}
}

